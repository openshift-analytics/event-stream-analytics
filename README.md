## Debugging

If you want to see what is happening on Kafka topic `openshift.builds.outputinfo` deploy a dumper: 

```
oc new-build -l=app=dumper python~https://github.com/elmiko/hacky-squawker.git --to=dumper

oc new-app --name dump-buildoutputinfo --labels=app=dumper --labels=dumper=buildoutputinfo --image-stream=dumper -e SERVERS=kafka:9092 -e TOPIC=openshift.builds.outputinfo
```
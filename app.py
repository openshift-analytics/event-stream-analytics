#!/usr/bin/env python

"""app.py: This is Event Stream Analytics, it will:
* identify completed Builds and emit Git info of that build
* log completed Builds for build duration prediction"""

__version__ = '0.2.0'

import os
import logging
import json
import urllib3
import time

from flask import Flask, jsonify
from urllib.parse import urlparse
from kafka import KafkaProducer
from threading import Thread

from EventStreamProcessor import *


app = Flask(__name__)

# Lets get our config from ENV
DEBUG_LOG_LEVEL = bool(os.getenv('DEBUG', False))
CONSUMED_TOPIC = os.getenv('CONSUMED_TOPIC', RAW_TOPIC_NAME)
# OpenShift
OPENSHIFT_API_ENDPOINT = 'https://127.0.0.1:8443/oapi/v1/'
BEARER_TOKEN = os.getenv('BEARER_TOKEN', 'UNSET')

repository = {}
repository['event'] = {}
observed_completed_builds = {}
buildsAndBuildInfoProcessor = None


@app.route('/')
def observations():
    return jsonify(buildSurvivalAnalysis.get_observations())


@app.route('/_healthz')
def healthz_liveliness():
    liveliness = {
        'buildsAndBuildInfoProcessor': buildsAndBuildInfoProcessor.is_alive(),
        'buildSurvivalAnalysis': buildSurvivalAnalysis.is_alive(),
    }

    if buildsAndBuildInfoProcessor.is_alive() and buildSurvivalAnalysis.is_alive():
        liveliness['OK'] = True
    else:
        liveliness['OK'] = False

    return jsonify(liveliness), liveliness['OK']


@app.route('/_healthz/readyness')
def healthz_readyness():
    if buildsAndBuildInfoProcessor.is_alive():
        return jsonify('READY'), 200
    else:
        return jsonify("NOTREADY"), 399


if __name__ == '__main__':
    """The main function
    This will launch the main Spark/Kafka stream processor class.
    """

    if DEBUG_LOG_LEVEL:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
        urllib3.disable_warnings()

    logger = logging.getLogger(__name__)

    logger.info('initializing...')

    # if we can read bearer token from /var/run/secrets/kubernetes.io/serviceaccount/token use it,
    # otherwise use the one from env
    try:
        logger.debug(
            'trying to get bearer token from secrets file within pod...')
        with open('/var/run/secrets/kubernetes.io/serviceaccount/token') as f:
            BEARER_TOKEN = f.read()

        # if we can read bearer token from file, use openshift.svc as anedpoint
        # otherwise use localhost
        OPENSHIFT_API_ENDPOINT = 'https://openshift.default.svc.cluster.local/oapi/v1/'

    except:
        logger.info("not running within an OpenShift cluster...")

    logger.debug("Using %s and Bearer token %s" %
                 (OPENSHIFT_API_ENDPOINT, BEARER_TOKEN))

    buildsAndBuildInfoProcessor = BuildsAndBuildInfoProcessor(
        OPENSHIFT_API_ENDPOINT, BEARER_TOKEN, topic=CONSUMED_TOPIC, bootstrap_servers=os.getenv('KAFKA_PEERS', 'localhost:9092'))

    buildSurvivalAnalysis = BuildSurvivalAnalysis(
        topic=BUILDS_COMPLETED_TOPIC_NAME, bootstrap_servers=os.getenv('KAFKA_PEERS', 'localhost:9092'))

    buildsAndBuildInfoProcessor.start()
    buildSurvivalAnalysis.start()

    logger.info('initialization done!')

    app.run(host='0.0.0.0', port=8080, debug=DEBUG_LOG_LEVEL)
